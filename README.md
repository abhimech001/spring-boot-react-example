# react-and-spring-data-rest

The application has a react frontend and a Spring Boot Rest API, packaged as a single module Maven application.
You can start the application (`./mvnw spring-boot:run`) and call the API by using the following curl (shown with its output):

---

\$ curl -v -u greg:turnquist localhost:8080/api/employees/3
{
"firstName" : "Frodo",
"lastName" : "Baggins",
"description" : "ring bearer",
"manager" : {
"name" : "greg",
"roles" : [ "ROLE_MANAGER" ]
},
"\_links" : {
"self" : {
"href" : "http://localhost:8080/api/employees/1"
}
}
}

---

To see the frontend, navigate to http://localhost:8080. You are immediately redirected to a login form. Log in as `greg/turnquist`


## Details about pipeline creation, infra creation and deletion
1) bitbucket-pipelines.yml - Pipeline as as code 

to build 
upload the artifacts to S3 
Use Cloud formation template to create the infrastructure and deploy and run the application.
### Update the user keys as secret in pipeline variable for AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY

2) EC2-Cloudformation-template.json
Cloud formation template to scale infra and deploy and run application.

3) How to delete cloud formation stack
Download and configure aws cli 

aws configure
AWS Access Key ID [None]: <access key id of user> 
AWS Secret Access Key [None]: < secret key of user> 
Default region name [None]: ap-southeast-1 
Default output format [None]: json

command to delete the cloud formation stack 
aws --region=ap-southeast-1 cloudformation delete-stack --stack-name demoCloudformationStack
